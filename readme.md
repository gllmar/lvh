
# Les voix humaines

Installation audiovisuelle immersive

![Les voix humaine, Larissa Corriveau en résidence @Normanville](img/2021_normanville/2021_residence.jpg)


## Synoptique

### Produit par l'artiste

#### 6 canaux visuel
* 3x Projections sur écrans  
* 3x Téléviseurs cathodique

#### 8 canaux sonore amplifés
* 3x écran  
* 3x télévision
* 1x Cerveau 
* 1x Subwoffer

![Signal fragmenté sur téléviseurs](img/2021_normanville/2021_television.jpeg)

### Fournis par la production

#### Sonore

Le silence dans une pièce acoustiquement traitée


#### Lumière

* Entrée tramisée
* Ambiance feutrée  
* Corridors de rideaux en velour noirs opaque et dense si nécessaire
* Noirceure totale dans la salle.
* Les banc; éclairés depuis un rubans de lumière douce, visible uniquement de l'arière.


#### Électricité

##### "Cerveau" de Monique
* 1x circuit 120v 13a 
  * acheminé via prise U-Ground au centre de la salle par un cable discret.


##### Bancs de théatre

* 1x prise 120v 
  * u-ground


#### Réseautique

* Internet filaire préférée, sans fil toléré

### Plantation

![Plantation cinematheque](schema/plantation.drawio.png)

 
#### Cerveau

![cerveau@go2022](img/2022_go/cerveau_go_side.jpeg)


![cerveau@go2022](img/2022_go/cerveau_go_centre.jpeg)


![cerveau](schema/patro_cerveau.drawio.png)


