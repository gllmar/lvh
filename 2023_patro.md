---
title: Les voix humaines
author: 
- "Larissa Corriveau"
- "Guillaume Arseneault (Lutherie numérique)"
- "Alex Hercule (Scénographie)" 

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \usepackage[francais]{babel}
    \fancyhf{Les voix humaines}
    \rhead{}
    \lhead{Synoptique}
    \lfoot{Patro 2023}
    \rfoot{Page \thepage}
---

# Les voix humaines

Installation audiovisuelle immersive

![Les voix humaine, Larissa Corriveau en résidence @Normanville](img/2021_normanville/2021_residence.jpg)

\tableofcontents

\newpage

## Synoptique


* 6 canaux visuel
  * 3 Projections sur surface courbe
    * concave, plane, concave
  * 3 Téléviseurs cathodique
    * moyen, petit et grand
* 8 canaux sonore
  * un par écran
  * un par télévision
  * Stéréo d'ambiance

![Signal fragmenté sur téléviseurs](img/2021_normanville/2021_television.jpeg)

\newpage

### Plantation

![Plantation préliminaire](schema/patro_plantation.drawio.png)

\newpage
 
#### Cerveau

![cerveau@go2022](img/2022_go/cerveau_go_side.jpeg)


![cerbeau@go2022](img/2022_go/cerveau_go_centre.jpeg)


![cerveau](schema/patro_cerveau.drawio.png)



\newpage

### Matériel

* ![1x PSU ampli](schema/ref/amplifier_psu.drawio.png)
* ![4x amplificateur](schema/ref/audio_amplifier.drawio.png)
* ![1x Audio interface](schema/ref/audio_interface.drawio.png)
* ![1x keystone rack](schema/ref/keystone_holder.drawio.png)
* ![8x keystone banana](schema/ref/keystone_type_banana.drawio.png)
* ![3x keystone RCA](schema/ref/keystone_type_RCA.drawio.png)
* ![1x kvm extender](schema/ref/KVM_extender.drawio.png)
* ![1x mac studio holder](schema/ref/mac_studio_holder.drawio.png)
* ![1x mac studio](schema/ref/mac_studio.drawio.png)
* ![1x switch poe 16 ports ](schema/ref/network_switch.drawio.png)
* ![3x projo](schema/ref/projo.drawio.png)
* ![1x rack 3u](schema/ref/rack_3u.drawio.png)
* ![1x powerbar rack](schema/ref/rack_powerbar.drawio.png)
* ![6x rca breakout](schema/ref/rca_breakout.drawio.png)
* ![1x routeur](schema/ref/router.drawio.png)
* ![1x terminal block](schema/ref/terminal_block.drawio.png)
* ![3x trrs breakout](schema/ref/trrs_breakout.drawio.png)


